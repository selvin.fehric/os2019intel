.globl main
.text
  main:

    pushl %ebp
    movl %esp, %ebp

    movl $5, %eax
    movl $6, %ebx
    cmpl %eax, %ebx
    jl L1
    movl $1, %ecx
    jmp L2
  L1:
    movl $1, %edx
  L2:

    movl $5, %eax
    movl $6, %ebx
    cmpl %ebx, %eax
    jl L3
    movl $1, %ecx
    jmp L4
  L3:
    movl $1, %edx
  L4:

    movl %ebp, %esp
    popl %ebp
    ret
