.data
.globl main
.text
  main:

  pushl %ebp
  movl %esp, %ebp

  movl $0, %eax
  movl $0, %ebx
  movb $2, %al
  movb $3, %ah
  movw $3, %bx
  movl $6, %ecx

  leal 0xffffcc24, %edx
  movl 0x080483ee(%ecx, %ebx,2), %eax
  movl $0x9a9b9c9d, %eax
  movl $0, %edx

  movzwl %ax, %ebx
  movsbl %al, %ecx
  movsbw %ah, %dx

  movl %ebp, %esp
  popl %ebp
  ret

