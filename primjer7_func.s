.section .data
 broj: .word 0

.section .rodata 
print_unos: .asciz "Unesite broj: "
scan_sim: .asciz "%d"
print_rezultat: .asciz "Rezultat: %d\n"


.section .text
.globl main 
main:
  pushl %ebp            #   Prolog main funkcije
  movl %esp, %ebp       #   Prolog main funkcije
  pushl $print_unos     #   Postavljanje na stack pointera na pocetak stringa print_unos kao argument za printf
  call printf           #   Pozivanje funkcije printf
  #
  addl $4, %esp           #vracanje za print_unos
  pushl $broj         #prvi arg za scanf
  pushl $scan_sim           #drugi arg za scanf
  call scanf              
  addl $8, %esp           #vracanje stacka od arg
  
  pushl broj             #push arg za fakt
  call faktorijel         
  addl $4, %esp           #vracanje stacka od arg

  pushl %eax   #prvi arg za prinf
  pushl $print_rezultat         #drugi arg/ret od fakt
  call printf
  addl $8, %esp           #vracanje stacka od arg
  #
  movl %ebp, %esp       #   Epilog main funkcije
  popl %ebp             #   Epilog main funkcije
  ret                   #   Return 

.globl faktorijel
faktorijel:
  pushl %ebp            #   Prolog faktorijel funkcije
  movl %esp, %ebp       #   Prolog faktorijel funkcije
  movl 8(%esp), %ebx    #   Preuzimanje argumenta sa stack-a
  #
  cmpl $1, %ebx           #provjera uslova
  jg rekurzija            #if(broj>1) goto
  movl $1, %eax           #return 1
  jmp kraj
rekurzija:
  pushl %ebx              #spremljen broj za pozvani fakt
  decl %ebx               #broj--
  pushl %ebx              #arg za poziv fakt
  call faktorijel
  addl $4, %esp           #vracanje stacka od arg
  popl %ebx               #vracanje broja za pozvani fakt
  mull %ebx               #rez=rez*broj

kraj:
  #
  movl %ebp, %esp       #   Epilog faktorijel funkcije
  popl %ebp             #   Epilog faktorijel funkcije
  ret


