.data
.globl main
.text
  main:

    pushl %ebp
    movl %esp, %ebp

    movl $0x12345678, %eax
    movl $0xabcddcba, %ebx
    movl %eax, -0x10(%esp)
    andl %eax, %ebx
    orw -0x10(%esp), %bx

    movl $0x12345678, %ecx
    notb %ch
    movl $0x12345678, %ecx
    notw %cx
    movl $0x12345678, %ecx
    notl %ecx

    movl $0xffffffff, %ecx
    negb %cl
    movl $0xffffffe0, %ecx
    negw %cx
    movl $0xfffff000, %ecx
    negl %ecx

    incb 0x1(%esp)
    incw 0x1(%esp)
    incl 0x1(%esp)
    incb %ah

    movl $0xffffffff, %edx
    incb %dl

    movl %ebp, %esp
    popl %ebp
    ret
