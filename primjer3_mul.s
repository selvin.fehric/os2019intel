.data
.globl main
.text
  main:

    pushl %ebp
    movl %esp, %ebp

    movl $100, %eax
    movl $100, %ebx
    movl $1000, %ecx
    movl $0, %edx

    mulb %bl
    mulw %cx
    mull %ecx

    movl $10, %eax
    movl $0x9f0, %ebx
    movl $0xff00, %ecx
    movl $0, %edx

    mulb %bl
    mulw %cx
    mull %ecx

    movl $10, %eax
    movl $0x9f0, %ebx
    movl $0xff00, %ecx
    movl $0, %edx

    imulb %bl
    negw %ax
    imulw %cx
    imull %ecx
    
    movl %ebp, %esp
    popl %ebp
    ret
